//
//  MyVisionkitProjectApp.swift
//  MyVisionkitProject
//
//  Created by 康錦豐 on 2022/8/23.
//

import SwiftUI

@main
struct MyVisionkitProjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
