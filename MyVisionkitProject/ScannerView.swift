//
//  ScannerView.swift
//  MyVisionkitProject
//
//  Created by 康錦豐 on 2022/8/24.
//

import SwiftUI
import VisionKit

struct ScannerView: UIViewControllerRepresentable {
    
    var finishScanning: ((_ result: Result<[UIImage], Error>) -> ())
    var cancelScanning: () -> ()
    
    func makeUIViewController(context: Context) -> VNDocumentCameraViewController {
        let controller = VNDocumentCameraViewController()
        controller.delegate = context.coordinator
        return controller
    }
    
    func updateUIViewController(_ uiViewController: VNDocumentCameraViewController, context: Context) {}
    
    func makeCoordinator() -> VNCameraCoordinator {
        VNCameraCoordinator(with: self)
    }
    
}

class VNCameraCoordinator: NSObject, VNDocumentCameraViewControllerDelegate {
    let scannerView: ScannerView
    
    init(with scannerView: ScannerView) {
        self.scannerView = scannerView
    }
    
    
    // MARK: - VNDocumentCameraViewControllerDelegate
    
    /// 用戶已成功從文檔相機保存掃描的文檔
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        var scannedPages = [UIImage]()
        
        for i in 0..<scan.pageCount {
            scannedPages.append(scan.imageOfPage(at: i))
        }
        
        scannerView.finishScanning(.success(scannedPages))
    }
    
    /// 用戶已從文檔掃描儀相機中取消
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        scannerView.cancelScanning()
    }
    
    /// 當相機視圖控制器處於活動狀態時，文檔掃描失敗
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        scannerView.finishScanning(.failure(error))
    }
}
