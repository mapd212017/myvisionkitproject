//
//  ContentView.swift
//  MyVisionkitProject
//
//  Created by 康錦豐 on 2022/8/23.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var itemContentModel = ContentItemModel()
    
    @State private var isShowScanner = false
    @State private var isRecognizing = false
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .bottom) {
                
                List(itemContentModel.items, id: \.id) { textItem in
                    NavigationLink(destination: TextPreviewView(text: textItem.text)) {
                        Text(String(textItem.text.prefix(50)).appending("..."))
                    }
                }
                
                if isRecognizing {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: Color(UIColor.systemIndigo)))
                        .padding(.bottom, 20)
                }
            }
            .navigationTitle("Scanner")
            .navigationBarItems(trailing: Button(action: {
                guard !isRecognizing else { return }
                isShowScanner = true
            }, label: {
                HStack {
                    Image(systemName: "doc.text.viewfinder")
                        .renderingMode(.template)
                        .foregroundColor(.white)
                    
                    Text("Scan")
                        .foregroundColor(.white)
                }
                .padding(.horizontal, 16)
                .frame(height: 36)
                .background(Color(UIColor.systemIndigo))
                .cornerRadius(18)
            }))
        }
        .sheet(isPresented: $isShowScanner) {
            ScannerView { result in
                switch result {
                    case .success(let scannedImages):
                        isRecognizing = true
                        
                        TextRecognition(scannedImages: scannedImages,
                                        recognizedContent: itemContentModel) {
                            // Text recognition is finished, hide the progress indicator.
                            isRecognizing = false
                        }
                        .recognizeText()
                        
                    case .failure(let error):
                        print(error.localizedDescription)
                }
                
                isShowScanner = false
            } cancelScanning: {
                // Dismiss the scanner controller and the sheet.
                isShowScanner = false
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
