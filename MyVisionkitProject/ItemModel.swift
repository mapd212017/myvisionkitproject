//
//  ItemModel.swift
//  MyVisionkitProject
//
//  Created by 康錦豐 on 2022/8/24.
//

import SwiftUI

class TextItem: Identifiable {
    var id: String
    var text: String = ""
    
    init() {
        id = UUID().uuidString
    }
}


class ContentItemModel: ObservableObject {
    @Published var items = [TextItem]()
}
